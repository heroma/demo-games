﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameTools
{
    public class ColorTools
    {


        private static float stringToFloat(string hex){
            return System.Convert.ToInt32(hex, 16) / 255f;
        }

        public static Color getColor(string hex){
            float r = stringToFloat(hex.Substring(0, 2));
            float g = stringToFloat(hex.Substring(2, 2));
            float b = stringToFloat(hex.Substring(4, 2));
            float a = 1f;
            if (hex.Length >= 8){
                a = stringToFloat(hex.Substring(6, 2));
            }
            return new Color(r, g, b);
        }


        public static bool compareColor(Color32 c1, Color32 c2){
            if (c1.r == c2.r && c1.g == c2.g && c1.b == c2.b){
                return true;
            }

            return false;
        }

	    public static string convertColorToStringHex(Color color){
            return string.Format("#{0:X2}{1:X2}{2:X2}", ToByte(color.r), ToByte(color.g), ToByte(color.b));
        }

        private static byte ToByte(float f){
            f = Mathf.Clamp01(f);
            return (byte)(f * 255);
        }
	}
}