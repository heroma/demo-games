﻿using UnityEngine;
using System.Collections;

public class ItemManager : MonoBehaviour {


    #region Singelton

    public static ItemManager instance;

    private void Awake() {
        if(instance != null) {
            Debug.LogWarning("More then one ItemManager instantiate!!!");
            return;
        }
        instance = this;
    }

    #endregion

    private ResourceList itemList; 
    // Use this for initialization
    void Start() {
        itemList = XmlFactory.readXml<ResourceList>("Resources");
        Debug.Log("LOAD ITEM Database");

    }
    
    public ResourceDto getItem(string objectid) {
        Debug.Log("Search for ITEM: "+objectid);
        foreach(ResourceDto dto in itemList.resourceDtos) {
            if(dto.id.Equals(objectid))
                return dto;
        }
        return null;
    }

    
   
}
