﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;

public class XmlFactory {
    /**
    *XML-FACTORY
    **/
    private static System.Xml.Serialization.XmlSerializer initSerializer<T>() {
        System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(T));
        return reader;
    }

    private static System.IO.StreamReader initReader(string file) {
	    return new System.IO.StreamReader(Application.persistentDataPath+"\\"+file+".xml");
    }

    private static System.IO.StreamWriter initWriter(string file) {
        return new System.IO.StreamWriter(Application.persistentDataPath + "\\" + file + ".xml");
    }

    public static void writeXml<T>(string fileName, System.Object o) {
        var writer = initSerializer<T>();
        var wfile = initWriter(fileName);
        writer.Serialize(wfile, o);
        wfile.Close();
    }

    public static T readXml<T>(string fileName) {
       var reader = initSerializer<T>();
       var file = initReader(fileName);

       T entity = (T)reader.Deserialize(file);
       file.Close();
       return entity;
    }

    public static void writeXml<T>(T item, String fileName) {
        var writer = new System.Xml.Serialization.XmlSerializer(typeof(T));
        var file = new System.IO.StreamWriter(@Application.dataPath + fileName);
        writer.Serialize(file, item);
        file.Close();
    }
}
