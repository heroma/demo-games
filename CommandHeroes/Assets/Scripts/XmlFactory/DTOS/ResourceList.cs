﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

public class ResourceList {

    [XmlArrayItem(ElementName = "Resource")]
    public List<ResourceDto> resourceDtos { get; set; }

   
    public ResourceList() {
        resourceDtos = new List<ResourceDto>(); 
    }

    public int count() {
        return resourceDtos.Count;
    }

    public void add(ResourceDto resource) {
        resourceDtos.Add(resource);
    }
    

}
