﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;


public class ResourceDto {

    [XmlAttribute("id")]
    public int id { get; set; }

    [XmlAttribute("name")]
    public string name { get; set; }

   
    [XmlAttribute("discription")]
    public string discription { get; set; }

    [XmlAttribute("gewicht")]
    public string gewicht { get; set; }

    [XmlAttribute("image")]
    public string image { get; set; }

    [XmlAttribute("type")]
    public string type { get; set; }

    //Inventorie object
    [XmlAttribute("effect")]
    public float effect { get; set; }

    public ResourceDto() { }

    public ResourceDto(int id, string name, string discription, float effect, string gewicht, string type, string image) {
        Debug.Log("Id: " + id);
        this.id = id;
        this.name = name;
        this.discription = discription;
        this.effect = effect;
        this.type = type;
        this.gewicht = gewicht;
        this.image = image;
    }
    
}
