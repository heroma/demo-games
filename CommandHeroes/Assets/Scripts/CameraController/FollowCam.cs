﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
	[SerializeField]
	private Transform player;

	[SerializeField]
	private Vector2 offset;

	[SerializeField]
	private float zoomSpeed = 2;
	
	private float speed = 1.5f;
	private float maxZoom = -6f;
	private float minZoom = -9f;
	
	private float minXPosition =-2.5f;
	private float maxXPosition = 6.5f;
	private float minYPosition =1.9f;
	private float maxYPosition =5.5f;
	private bool goToFig = true;
	private float zoom = -6f;
	private Plane plane;
	private Vector3 newPos = new Vector3(-1.5f, 2.02f,-12f);
    
	void Update()
	{
		if(player != null && goToFig){
			newPos = new Vector3(player.position.x+offset.x,player.position.y+offset.y,-12); 
		}
	
		if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer){
			//Move
			if(Input.GetKey(KeyCode.W)){
				newPos += (Vector3.up) * speed * Time.deltaTime;
				goToFig = false;
			}
			if(Input.GetKey(KeyCode.S)){
				newPos += (-Vector3.up) * speed * Time.deltaTime;
				goToFig = false;
			}
			if(Input.GetKey(KeyCode.D)){
				newPos += (Vector3.right) * speed * Time.deltaTime;
				goToFig = false;
			}
			if(Input.GetKey(KeyCode.A)){
				newPos +=(-Vector3.right) * speed * Time.deltaTime;
				goToFig = false;
			}
		
			newPos = new Vector3 (newPos.x,newPos.y,handelZoom());
		}else if(Application.platform == RuntimePlatform.Android){
			
			if(Input.touchCount == 1){
				if(Input.GetTouch(0).deltaPosition.x > 0 && newPos.x < maxXPosition) {
					newPos += (Vector3.right) * speed * Time.deltaTime;
					goToFig = false;
				}
				if(Input.GetTouch(0).deltaPosition.x < 0 && newPos.x > minXPosition){
					newPos +=(-Vector3.right) * speed * Time.deltaTime;
					goToFig = false;
				}
				if(Input.GetTouch(0).deltaPosition.y > 0 && newPos.y < maxYPosition){
					newPos += (Vector3.up) * speed * Time.deltaTime;
					goToFig = false;
				}
				if(Input.GetTouch(0).deltaPosition.y < 0 && newPos.y > minYPosition){
					newPos += (-Vector3.up) * speed * Time.deltaTime;
					goToFig = false;
				}
			}
			
			newPos = handelZoomAndroid(newPos.x,newPos.y);
		}
	
		Camera.main.transform.position = newPos;
	}
	
	private float handelZoom(){
		if (Input.GetAxis("Mouse ScrollWheel") > 0f ) {
			if(zoom > minZoom){
				zoom -= 0.5f;
			}
		}else if (Input.GetAxis("Mouse ScrollWheel") < 0f ){
			if(zoom < maxZoom){
				zoom +=0.5f;
			}
		}
		return zoom * zoomSpeed;
	}

	private Vector3 planePosition(Vector2 screenPos){
		var rayNow = Camera.main.ScreenPointToRay(screenPos);
		if(plane.Raycast(rayNow,out var enterNow)){
			return rayNow.GetPoint(enterNow);
		}
		return Vector3.zero;
	}
	
	private Vector3 handelZoomAndroid(float posx,float posy){
		
		if(Input.touchCount == 2){
			Touch touch1 = Input.GetTouch(0);
			Touch touch2 = Input.GetTouch(1);
			Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
			Vector2 touch2PrevPos = touch2.position - touch2.deltaPosition;
			float prevMagnitude = (touch1PrevPos -touch2PrevPos).magnitude;
			float currentMagnitude = (touch1.position - touch2.position).magnitude;
			float diverence = currentMagnitude - prevMagnitude;
			
			if(diverence > 0){
				if(zoom < maxZoom){
					zoom += 0.2f;
				}	
			}else{
				if(zoom > minZoom){
					zoom -= 0.2f;
				}
			}
			return new Vector3(posx,posy,zoom * zoomSpeed);
		}
		
		
		return new Vector3(posx,posy,zoom * zoomSpeed);
	}
    
	public void setPlayer(Transform player){
		goToFig = true;
		this.player = player; 
	}
}
