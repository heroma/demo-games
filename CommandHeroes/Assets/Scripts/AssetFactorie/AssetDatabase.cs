﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameTools
{
    public class AssetDatabase : MonoBehaviour
    {
        [Serializable]
        private class AssetItem
        {
            public string id;
            public GameObject gameObject;

        }
        [SerializeField]
        private AssetItem[] assetDB;

        private static AssetDatabase _instance;
        public static AssetDatabase instance {
            get {
                if (_instance == null)
                {
                    _instance = Instantiate(Resources.Load<AssetDatabase>("AssetDatabase"));
                }
                return _instance;
            }
        }

        private void Awake()
        {
            this.transform.SetParent(GameObject.Find("GameManager").transform);
        }

        public GameObject getGameObject(String id)
        {
            foreach (AssetItem item in assetDB)
            {
                if (item.id.Equals(id))
                    return item.gameObject;
            }
            Debug.LogError("Es wurde kein Item gefunden! ID[" + id + "]");
            return null;
        }
    }
}