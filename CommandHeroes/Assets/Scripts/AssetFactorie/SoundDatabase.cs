﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundDatabase : MonoBehaviour
{
    [Serializable]
    public class AudioItem
    {
        public string id;
        public AudioClip audioClip;
	    public float timer;
	    [Range(0,1)]
	    public float volume = 10f;
    }
    [SerializeField]
    private AudioItem[] assetDB;

    private static SoundDatabase _instance;
    public static SoundDatabase instance {
        get {
            if (_instance == null)
            {
                _instance = Instantiate(Resources.Load<SoundDatabase>("SoundDatabase"));
            }
            return _instance;
        }
    }

    private void Awake()
    {
        this.transform.SetParent(GameObject.Find("GameManager").transform);
    }

    public AudioItem getAoudioClip(string id)
    {
        foreach (AudioItem item in assetDB)
        {
            if (item.id.Equals(id))
                return item;
        }
        Debug.LogError("["+ instance.name+"]Es wurde kein Item gefunden! ID[" + id + "]");
        return null;
    }
}

