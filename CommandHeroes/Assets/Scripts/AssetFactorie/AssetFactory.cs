﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTools
{
    public class AssetFactory : MonoBehaviour
    {
        public static GameObject loadAssetFromDatabase(string assetID)
        {
            return Instantiate(AssetDatabase.instance.getGameObject(assetID));
        }

        public static SoundDatabase.AudioItem loadAudioClipFromDatabase(string clipID)
        {
           return SoundDatabase.instance.getAoudioClip(clipID);
        }

        public static void loadPartikelFromDatabase(string partikelID,Vector3 postion,float rotation,Vector3 size)
        {
	        //     PartikelDatabase.PartikelItem item = PartikelDatabase.instance.getPartikel(partikelID);
	        // PartikelSystem.instance.spawnPartikel( new PartikelSystem.PartikelSystemArgs(postion, rotation, size, item.material));
        }

    }
}