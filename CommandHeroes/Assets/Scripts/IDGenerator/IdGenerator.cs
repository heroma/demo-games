﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameTools{

	public class IdGenerator 
	{
		public static string createId(){
			string id = ""+Guid.NewGuid();
			Debug.Log("Generate-ID: "+id);
			return id;
		}
		
		public static string createId(int lenght){
			string id = ""+Guid.NewGuid().ToString().Replace("-","").Substring(0,lenght);
			Debug.Log("Generate-ID: "+id);
			return id;
		}
	}
}
