﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameTools;
using System.IO;
public class GameManager : MonoBehaviour {
   
	/*---------------------------------------
			Takes care of game logic
	-----------------------------------------*/
   
	private static Dictionary<string,Transform> figurenDic;
	private static string selectedFigurId;
	private static FigurenChooser chooser;
	private const int MAX_LEVEL = 6; 
	private static Trash trash;
	private static int ACTION_COUNTER = 0;
	private static Dictionary<string,bool> actionIsRunningDic;
	
	protected void Awake(){
		chooser = GameObject.FindObjectOfType<FigurenChooser>();
		figurenDic = new Dictionary<string, Transform>();
		actionIsRunningDic = new Dictionary<string, bool>();
		SoundManager.playSound("BG_SOUND",true);
		trash = GameObject.FindObjectOfType<Trash>();
		if(!File.Exists(Application.persistentDataPath+"\\"+"LevelCountData.xml")){
			LevelFinish.initDaten();
		}
	}
	
	public static void addFigure(string id,Transform prefap){
		if(!figurenDic.ContainsKey(id)){
			figurenDic.Add(id,prefap);
		}
		
		if(CommandSystem.CommandManager.getUiManager() != null){
			CommandSystem.CommandManager.getUiManager().addUI(id,prefap.GetComponentInChildren<CharUiTag>().transform);
		}else{
			Debug.Log("CommandSystem.CommandManager.getUiManager() == null");
		}
		actionIsRunningDic.Add(id,false);
	}
    
	public static Dictionary<string,Transform> getFigurenDic(){
		return figurenDic;
	}
    
	public static Transform getFigure(string id){
		selectedFigurId = id;
		Debug.Log("selectedFigurId: "+selectedFigurId);
		if(id != null){
			figurenOutline(id);
			return figurenDic[id];
		}
		return null;
	}
    
	private static void figurenOutline(string id){
		Outline outline = null;
		foreach(Transform t in figurenDic.Values){
			outline = t.GetChild(0).gameObject.GetComponent<Outline>();
			if(outline != null){
				outline.enabled = false;
			}
		}
		outline = figurenDic[id].GetChild(0).gameObject.GetComponent<Outline>();
		if(outline != null){
			outline.enabled = true;
		}
	}
    
	public static string getSelectedId(){
		return selectedFigurId;
	}
    
	public void reloadGame(){
		LevelFinish.updateLevelTrys();
		FigurenChooser.clearSelectedFigure();
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		clearCounter();
	}
	
	public static Transform getTrash(){
		return trash.transform;
	}
	
	public static void loadNext(){
		Debug.Log("Scene: "+SceneManager.GetActiveScene().buildIndex+"/"+SceneManager.sceneCount);
		clearCounter();
		if(SceneManager.GetActiveScene().buildIndex+1 <= MAX_LEVEL){
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
			
		}else{
			SceneManager.LoadScene(1);
		}
		LevelFinish.updateLevelTrys();
	}
	
	public static int getLevelNumber(){
		return SceneManager.GetActiveScene().buildIndex;
	}
	
	protected void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			exitGame();
		}		
	}
	
	public static void clearCounter(){
		ACTION_COUNTER = 0;
	}
	
	public static int getCounter(){
		return ACTION_COUNTER;
	}
	
	public static void increasCounter(){
		ACTION_COUNTER++;
	}
	
	public static void setActionIsRunning(string id){
		actionIsRunningDic[id] = true;
		
	}
	
	public static void setActionIsStop(string id){
		actionIsRunningDic[id] = false;
	} 
	
	private void stopsAllActions(){
		foreach( string key in actionIsRunningDic.Keys){
			actionIsRunningDic[key] = false;
		}
	}
	
	public static bool isActionRunning(){
		foreach(bool b in actionIsRunningDic.Values){
			if(b){
				return true;
			}	
		}
		return false;
	}
	
	public void exitGame(){
			#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
			#else
		Application.Quit();
			#endif
	}
}
