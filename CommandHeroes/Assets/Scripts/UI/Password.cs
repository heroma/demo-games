﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class Password : MonoBehaviour
{
	[SerializeField]
	private Transform password;
	
	private const string PASSWORD = "myDemo2022";
	
	public void checkPassword(){
		if(!password.GetComponent<TMP_InputField>().text.Equals(PASSWORD)){
			Application.Quit();
		}else{
			SceneManager.LoadScene("Scenes/Start_Menu");
		}
	}
}
