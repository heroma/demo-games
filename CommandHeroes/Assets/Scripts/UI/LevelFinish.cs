﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using TMPro;
public class LevelFinish : MonoBehaviour {
   

	private static CounterList dataList;

	[SerializeField]
	private Transform counter;

	[SerializeField]
	private Transform try1;
	
	[SerializeField]
	private Transform try2;
	
	[SerializeField]
	private Transform try3;
	
	
	protected void Start(){
		dataList = new CounterList();
	}
   
	public void load(int level){
		dataList = XmlFactory.readXml<CounterList>("LevelCountData");
		fillTrysForLevel(level);
	}
   
	public static void write(){
		XmlFactory.writeXml<CounterList>("LevelCountData",dataList);
		foreach(ComandCountDTO dto in dataList.getList()){
			foreach(TryDTO t in dto.trys){
				Debug.Log(dto.level+" "+t.count+" "+t.datum);
			}
		}
	}
   
	public static void initDaten(){
		dataList = new CounterList();
		Debug.Log("Init Protokoll");
		for(int i=1;i<6;i++){
			ComandCountDTO dto = new ComandCountDTO();
			dto.level = i;
			dto.trys = new List<TryDTO>(){new TryDTO(System.DateTime.Now.ToShortDateString(),250),new TryDTO(System.DateTime.Now.ToShortDateString(),250),new TryDTO(System.DateTime.Now.ToShortDateString(),250)} ;
			dataList.getList().Add(dto);
		
		}
		write();
	}
   
	public void fillTrysForLevel(int level){
		counter.GetComponent<TMPro.TextMeshProUGUI>().SetText(""+GameManager.getCounter());
		
		foreach(ComandCountDTO dto in dataList.getList()){
			if(dto.level == level){
				try1.GetComponent<TMPro.TextMeshProUGUI>().SetText(""+dto.trys[0].datum +" - "+dto.trys[0].count);
				try2.GetComponent<TMPro.TextMeshProUGUI>().SetText(""+dto.trys[1].datum +" - "+dto.trys[1].count);
				try3.GetComponent<TMPro.TextMeshProUGUI>().SetText(""+dto.trys[2].datum +" - "+dto.trys[2].count);
			}
		}
	}

	private static void updateTryForLevel(int level, TryDTO newTry){
		TryDTO removeTry = null;
		if(dataList == null || dataList.getList() == null){
			dataList = XmlFactory.readXml<CounterList>("LevelCountData");
		}
		
		foreach(ComandCountDTO dto in dataList.getList()){
			if(dto.level == level){
				foreach(TryDTO t in dto.trys){
					if(newTry.count < t.count){
						removeTry = t;
					}
				}
				if(removeTry != null){
					dto.trys.Remove(removeTry);
					removeTry = null;
					dto.trys.Add(newTry);
					Debug.Log("Ersetze DTO --count: "+newTry.count);
				}
			}
		}
		write();
	}

	public static void updateLevelTrys(){
		if(GameManager.getCounter() > 0){
			updateTryForLevel(GameManager.getLevelNumber(), new TryDTO(System.DateTime.Now.ToShortDateString(),GameManager.getCounter()));
		}
	}

	public void setCounter(int count){
		counter.GetComponent<TMPro.TextMeshProUGUI>().SetText(""+count);
	}
}
