﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
	[SerializeField]
	private Transform infoPanel;
   
   
	public void showInfoPanel(){
		infoPanel.gameObject.SetActive(!infoPanel.gameObject.activeSelf);
	}
}
