﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FigurenChooser : MonoBehaviour {
	
	private const string mageId = "char_mage";
	private const string elfId = "char_elf";
	private const string knightId = "char_knight";
	[SerializeField]
	private Button button_mage;
	[SerializeField]
	private Button button_knight;
	[SerializeField]
	private Button button_elf;
	
	public void createPictures(string id,Transform figure ){
		
		Debug.Log("Createt Sprite id: "+id+" | gameObject: "+figure.gameObject.name);
		Sprite sprite = PictureMaker.Instance.getSpriteFromObject(figure.gameObject,true);
		if(id.Equals(mageId)){
				button_mage.GetComponent<Image>().sprite = sprite;
				button_mage.image.sprite = sprite;
		}else if(id.Equals(knightId)){
				button_knight.GetComponent<Image>().sprite = sprite;
				button_knight.image.sprite = sprite;
		}else if(id.Equals(elfId)){
				button_elf.GetComponent<Image>().sprite = sprite;
				button_elf.image.sprite = sprite;
			}
		
	}
	
	public static void setFigure(string id){
		if(id.Equals(mageId)){
			setFigure(Figure.MAGE);
		}else if(id.Equals(elfId)){
			setFigure(Figure.ELF);
		}else if(id.Equals(knightId)){
			setFigure(Figure.KNIGHT);
		}
	}
	
	public static void clearSelectedFigure(){
		CommandSystem.CommandManager.setPlayer(null);
	}
   
	private static void setFigure(Figure fig){
		
		switch(fig){
			case Figure.MAGE :{
			Transform mage = GameManager.getFigure(mageId);
			Camera.main.GetComponent<FollowCam>().setPlayer(mage);
			CommandSystem.CommandManager.setPlayer(mageId);
			break;			
			} 
		case Figure.ELF :{
			Transform elf = GameManager.getFigure(elfId);
			Camera.main.GetComponent<FollowCam>().setPlayer(elf);
			CommandSystem.CommandManager.setPlayer(elfId);
			break;			
			} 
		case Figure.KNIGHT :{
			Transform knight = GameManager.getFigure(knightId);
			Camera.main.GetComponent<FollowCam>().setPlayer(knight);
			CommandSystem.CommandManager.setPlayer(knightId);
			break;			
			} 
		}
	}
   
	public bool allIn(){
		return (!get_mage_btn().IsActive() &&  !get_elf_btn().IsActive() && !get_knight_btn().IsActive());
	}
   
	public Button get_mage_btn(){
		return button_mage;
	}
   
   
	public Button get_elf_btn(){
		return button_elf;
	}
	
	
	public Button get_knight_btn(){
		return button_knight;
	}
   
	private enum Figure{
		MAGE,ELF,KNIGHT
	}
}
