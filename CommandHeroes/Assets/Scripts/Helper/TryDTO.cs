﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
public class TryDTO {
   
	[XmlAttribute("count")]
	public int count {get;set;}
	
	[XmlAttribute("rang")]
	public string datum {get;set;}
   
	public TryDTO(){}
	
	public TryDTO(string datum, int count){
		this.datum = datum;
		this.count = count;
		
	}
	
}
