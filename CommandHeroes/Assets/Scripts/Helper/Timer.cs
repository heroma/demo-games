﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Timer : MonoBehaviour{
	private float timer;
	private static bool start = false;
	private TextMeshProUGUI textObject;
	private 
	
	protected void Start(){
		textObject = GetComponent<TextMeshProUGUI>();
		startTimer();
	}
	
    void Update(){
	    if(start){
		    timer += Time.deltaTime;
		    textObject.text = Mathf.Round(getTimeInMinutes())+":"+Mathf.Round(getTimeInSeconds()); 
	    }
    }
    
	public float getTimeInSeconds(){
		return timer % 60;
	}
	
	public float getTimeInMinutes(){
		return timer / 60; 
	}
	
	public void resetTimer(){
		timer = 0.00f;
	}
	
	public static void startTimer(){
		start = true;
	}
	public static void stopTimer(){
		start = false;
	}
}
