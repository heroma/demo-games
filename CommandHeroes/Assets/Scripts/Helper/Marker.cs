﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marker : MonoBehaviour {
	private string actionId;
	
	
	protected void OnTriggerEnter(Collider other) {
    	if(other.gameObject.tag == "Player"){
    		gameObject.SetActive(false);
    		transform.SetParent(GameManager.getTrash());
    	}
    }
	public void setActionId(string actionId){
		this.actionId = actionId;
	}
	
	public string getActionId(){
		return actionId;
	}
	
	
}
