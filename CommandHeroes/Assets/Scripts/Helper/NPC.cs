﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
	private CharacterController characterController;
	private Animator animator;
	
	private Vector3 direction;	
	void Start(){
		characterController = GetComponent<CharacterController>();
		animator = GetComponent<Animator>();
		direction = Vector3.left;
		StartCoroutine(moveNpc());
    }

	private IEnumerator moveNpc(){
		characterController.stopWalking();
		if(direction == Vector3.left){
			characterController.move_left();
		}else{
			characterController.move_right();
		}
		yield return new WaitForSeconds(1);
	}
	
	public void setDirection(Vector3 dir){
		direction = dir;
	}
	
	public Vector3 getDirection(){
		return direction;
	}
	
	public void changeDirection(){
		StopCoroutine(moveNpc());
		if(direction == Vector3.left){
			direction = Vector3.right;
		}else{
			direction = Vector3.left;
		}
		StartCoroutine(moveNpc());
	}
	
	public void die(){
		StartCoroutine(dieNPC());
	}
	
	private IEnumerator dieNPC(){
		animator.SetBool("Dead",true);
		yield return new WaitForSeconds(1f);
		transform.gameObject.SetActive(false);
	}
}
