﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullidTime : MonoBehaviour
{
	private static bool start = false;


	private float defTimeScale;	
	private float defFixtDelta;

	// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	protected void Start()
	{
		defTimeScale = Time.timeScale;
		defFixtDelta = Time.fixedDeltaTime;
	}
    // Update is called once per frame
    void Update()
    {
	    if(start){
	    	Time.timeScale = 0.1f;
	    	Time.fixedDeltaTime = Time.timeScale * 0.02f;
	    }else{
	    	Time.timeScale = defTimeScale;
		    Time.fixedDeltaTime = defFixtDelta;
	    }
    }
    
	public static void bulledTimeStart(){
		start = true;
	}
	public static void bulledTimeStop(){
		start = false;
	}
}
