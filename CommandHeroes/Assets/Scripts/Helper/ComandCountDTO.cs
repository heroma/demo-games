﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
public class ComandCountDTO {
	
	[XmlAttribute("level")]
	public int level {get;set;}
	
	[XmlArrayItem(ElementName = "try")]
	public List<TryDTO> trys {get;set;}
	
}

