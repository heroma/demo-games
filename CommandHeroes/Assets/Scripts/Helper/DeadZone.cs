﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
	private NPC npc;
	
	// Awake is called when the script instance is being loaded.
	protected void Awake(){
		npc = GetComponentInParent<NPC>();
	}
	
	protected void OnTriggerEnter(Collider other){
		Debug.Log("Dead Zone");	
		npc.die();
	}
}
