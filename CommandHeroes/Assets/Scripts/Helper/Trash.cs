﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour {
	
	/*---------------------------------------
	  Takes care of deletion of gameobjects
	-----------------------------------------*/
	
	[SerializeField]
	private float timeToDeleteInSeconds;
	[SerializeField]
	private bool checkIsDeaktiviert = false;

	private float time =0;
	
    void Update(){
	    time += Time.deltaTime;
	    if(time % 60 >= timeToDeleteInSeconds){
	    	for(int i =0;i < transform.childCount;i++){
	    		if(checkIsDeaktiviert){
	    			if(!transform.GetChild(i).gameObject.activeInHierarchy){
		    			GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
		    		}
	    		}else{
		    		GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
		    	}
	    	}
	    	time = 0;
	    }
    }
}
