﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
public class CounterList {
	
	[XmlArrayItem(ElementName = "ComandCount")]
	public List<ComandCountDTO> comandCountList;
    
	public CounterList(){
		comandCountList= new List<ComandCountDTO>();
		/*
		ComandCountDTO count = new ComandCountDTO();
		count.count = 5;
		count.level = 2;
		comandCountList.Add(count);
		*/
	}
    
	public List<ComandCountDTO> getList(){
		return comandCountList;
	}
	
	
    
}


