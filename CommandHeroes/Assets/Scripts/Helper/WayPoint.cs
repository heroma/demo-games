﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour{

	/*---------------------------------------
			Npc Waypoint / border
	-----------------------------------------*/

	private const string NPC_TAG = "NPC";

	protected void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == NPC_TAG){
		other.transform.rotation = Quaternion.Inverse(other.transform.rotation);
			other.GetComponent<NPC>().changeDirection();
		}
	}
}
