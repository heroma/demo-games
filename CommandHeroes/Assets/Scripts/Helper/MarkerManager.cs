﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerManager : MonoBehaviour{
    
	private static Dictionary<string,Dictionary<string,int>> param = new Dictionary<string, Dictionary<string,int>>();
	private const string RIGHT = "RIGHT"; 
	private const string LEFT = "LEFT"; 
    
	private static void iniDict(string playerId){
		if(!param.ContainsKey(playerId)){
			Dictionary<string,int> dic = new Dictionary<string, int>();
			dic.Add(RIGHT,1);
			dic.Add(LEFT,1);
			param.Add(playerId,dic);
		}
	}
    
	public static void decreaseCountRight(string playerId){
		iniDict(playerId);
		Dictionary<string,int> dic = param[playerId];
		dic[RIGHT]--;
	}
	
	public static void increaseCountRight(string playerId){
		iniDict(playerId);
		Dictionary<string,int> dic = param[playerId];
		dic[RIGHT]++;
	}
	
	public static void decreaseCountLeft(string playerId){
		iniDict(playerId);
		Dictionary<string,int> dic = param[playerId];
		dic[LEFT]--;
	}
	
	public static void increaseCountLeft(string playerId){
		iniDict(playerId);
		Dictionary<string,int> dic = param[playerId];
		dic[RIGHT] ++;
	}
	
	public static int getCountRight(string playerId){
		iniDict(playerId);
		Dictionary<string,int> dic =  param[playerId];
		return dic[RIGHT];
	}
	public static int getCountLeft(string playerId){
		iniDict(playerId);
		Dictionary<string,int> dic = param[playerId];
		return dic[LEFT];
	}
    
}
