﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ziel : MonoBehaviour
{
	private FigurenChooser figurenChooser;
	
	private static string mageId = "micro_wizard";
	private static string elfId = "micro_druid";
	private static string knightId = "micro_knight";
	
	public Transform levelFinishUI;
	
	protected void Start(){
		figurenChooser = GameObject.FindObjectOfType<FigurenChooser>();
	}
   
	protected void OnTriggerEnter(Collider other){
		if(other.transform.name.Contains(elfId)){
			figurenChooser.get_elf_btn().gameObject.SetActive(false);
			GameManager.setActionIsStop("char_elf");
		}
		if(other.transform.name.Contains(mageId)){
			figurenChooser.get_mage_btn().gameObject.SetActive(false);
			GameManager.setActionIsStop("char_mage");
		}
		if(other.transform.name.Contains(knightId)){
			figurenChooser.get_knight_btn().gameObject.SetActive(false);
			GameManager.setActionIsStop("char_knight");
		}
		other.transform.gameObject.SetActive(false);
		
	}
	private void showLevelEndDialog(){
		levelFinishUI.GetComponent<LevelFinish>().load(GameManager.getLevelNumber());
		levelFinishUI.gameObject.SetActive(true);
	}
	
	private void Update(){
		if(figurenChooser.allIn()){
			showLevelEndDialog();
		}
	}
}
