﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePot : MonoBehaviour
{

	protected void OnTriggerEnter(Collider other){
		Debug.Log("Player on TimePot");
		if(other.tag == "Player"){
			other.GetComponent<CharacterController>().stopWalking();
			Timer.stopTimer();
			BullidTime.bulledTimeStart();
		}
	}
	
	protected void OnTriggerExit(Collider other){
		Debug.Log("Player exit TimePot");
		if(other.tag == "Player"){
			
			Timer.startTimer();
			BullidTime.bulledTimeStop();
		}
	}
}
