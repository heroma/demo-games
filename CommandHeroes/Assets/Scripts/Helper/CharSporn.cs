﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTools;
public class CharSporn : MonoBehaviour {
	
	[SerializeField]
	private string charID;
   
	void Start(){
	    GameObject ob = AssetFactory.loadAssetFromDatabase(charID);
	    ob.transform.position = transform.position;
	    GameManager.addFigure(charID,ob.transform); 
    }
}
