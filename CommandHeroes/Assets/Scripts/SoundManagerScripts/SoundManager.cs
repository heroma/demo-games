﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTools
{
	public static class SoundManager
	{

		private static Dictionary<string,float> soundTimerDict = new Dictionary<string,float>();

		private static Dictionary<string,GameObject> soundDict = new Dictionary<string,GameObject>();

		private static GameObject soundOnShotGameObject;
		private static GameObject soundGameObject;

		public static void playSound(string clipId)
		{
            
			SoundDatabase.AudioItem audioItem = AssetFactory.loadAudioClipFromDatabase(clipId);
			if (audioItem != null && canPlaySound(clipId, audioItem)){
				if (soundOnShotGameObject == null)
				{
					soundOnShotGameObject = createGameObject(clipId);
					soundOnShotGameObject.AddComponent<AudioSource>();
				}
				AudioSource audioSource = soundOnShotGameObject.GetComponent<AudioSource>();
				audioSource.PlayOneShot(audioItem.audioClip);
			}
		}

		public static void playSoundOnSoundObject(string clipId)
		{
			SoundDatabase.AudioItem audioItem = createAudioItem(clipId);
			if (audioItem != null){
				GameObject newObject = createGameObject(clipId);
				newObject.AddComponent<AudioSource>();
				AudioSource audioSource = newObject.GetComponent<AudioSource>();
				audioSource.PlayOneShot(audioItem.audioClip);
			}
		 
		}

		private static SoundDatabase.AudioItem createAudioItem(string id){
			SoundDatabase.AudioItem audioItem = AssetFactory.loadAudioClipFromDatabase(id);
			return audioItem;
		}
	    
	   

		public static void playStoredSound(string id){
			if(soundDict.ContainsKey(id)){
				GameObject go = soundDict[id];
				go.GetComponent<AudioSource>().Play();
			}else{
				SoundDatabase.AudioItem audioItem = createAudioItem(id);
				if (audioItem != null){
					GameObject newObject = createGameObject(id);
					newObject.AddComponent<AudioSource>();
					AudioSource audioSource = newObject.GetComponent<AudioSource>();
					audioSource.clip = audioItem.audioClip;
					audioSource.Play();
					soundDict.Add(id,newObject);
				}
			}
		}

		public static void stopSound(string clipId){
			GameObject go = GameObject.Find("Sound["+clipId+"]");
			if(go != null){
				AudioSource audioSource = go.GetComponent<AudioSource>();
				audioSource.Stop();
				Object.Destroy(go);
			}
		}
	    
		public static void playSound(string clipId, bool loop)
		{

			SoundDatabase.AudioItem audioItem = AssetFactory.loadAudioClipFromDatabase(clipId);
			if (audioItem != null && canPlaySound(clipId, audioItem))
			{
				if (soundGameObject == null)
				{
					soundGameObject = createGameObject(clipId);
					soundGameObject.AddComponent<AudioSource>();
				}
				AudioSource audioSource = soundGameObject.GetComponent<AudioSource>();
			    
				audioSource.clip = audioItem.audioClip;
				audioSource.loop = loop;
				audioSource.volume = audioItem.volume;
				audioSource.Play();
				if(!loop){
					Object.Destroy(soundGameObject, audioItem.audioClip.length);
				}
			}
		}

		public static void playSound(string clipId, Vector3 position)
		{

			SoundDatabase.AudioItem audioItem = AssetFactory.loadAudioClipFromDatabase(clipId);
			if (audioItem != null && canPlaySound(clipId, audioItem))
			{
				if (soundGameObject == null)
				{
					soundGameObject = createGameObject(clipId);
					soundGameObject.AddComponent<AudioSource>();
				}
				AudioSource audioSource = soundGameObject.GetComponent<AudioSource>();
				soundGameObject.transform.position = position;
				audioSource.clip = audioItem.audioClip;
				audioSource.Play();

				Object.Destroy(soundGameObject, audioItem.audioClip.length);
			}
		}

		private static GameObject createGameObject(string clipId)
		{
			if (!soundTimerDict.ContainsKey(clipId))
			{
				soundTimerDict.Add(clipId, 0);
			}
			return new GameObject("Sound[" + clipId + "]");
		}

		private static bool canPlaySound(string clipId, SoundDatabase.AudioItem audioItem)
		{
			if (soundTimerDict.ContainsKey(clipId))
			{
				if (audioItem.timer == 0)
					return true;

				float lastPlayed = soundTimerDict[clipId];
				if(audioItem.timer + lastPlayed < Time.time)
				{
					soundTimerDict[clipId] = Time.time;
					return true;
				}
				else
				{
					return false;
				}
			}
			return true;
		}
	}
}
