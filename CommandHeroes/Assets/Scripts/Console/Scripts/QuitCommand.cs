﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DevelopmentConsole {

    public class QuitCommand : AbstractCommand
    {
        public override string name { get; protected set; }
        public override string command { get; protected set; }
        public override string description { get; protected set; }
        public override string help { get; protected set; }

        public QuitCommand()
        {
            name = "Quit";
            command = "quit";
            description = "Quit the application.";
            help = "Call this with no arguments to quit the Application.";

            addCommandToConsole();
        }

        public override void runCommand()
        {
            if (Application.isEditor)
            {
                #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                #endif    
            }
            else
            {
                Application.Quit();
            }
        }

        public static QuitCommand createCommand()
        {
            return new QuitCommand();
        }
    }
}
