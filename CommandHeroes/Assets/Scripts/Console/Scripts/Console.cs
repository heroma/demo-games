﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DevelopmentConsole {

    public abstract class AbstractCommand
    {
        public abstract string name { get; protected set; }
        public abstract string command { get; protected set; }
        public abstract string description { get; protected set; }
        public abstract string help  { get; protected set; }

        public void addCommandToConsole()
        {
            string addMessage = " command has been added to console.";

            Console.addCommandToConsole(command, this);
            Debug.Log(name + addMessage);
        }

        public abstract void runCommand();

    }

    public class Console : MonoBehaviour
    {
        public static Console instance { get; private set; }
        public static Dictionary<string, AbstractCommand> commands { get; private set; }

        [Header("UIComponents")]
        public Canvas canvas;
        public Text consoleText;
        public Text inputText;
        public InputField inputField;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            commands = new Dictionary<string, AbstractCommand>();
        }

        private void Start()
        {
            canvas.gameObject.SetActive(false);
            createCommand();
        }

        private void OnEnable()
        {
            Application.logMessageReceived += handleLog;
        }

        private void OnDisable()
        {
            Application.logMessageReceived -= handleLog;
        }

        private void handleLog(string logMessage,string lockTrace,LogType typ)
        {
            string message = "[" + typ.ToString() + "] "+logMessage;
            addMessage(message);
        }

        private void createCommand()
        {
            QuitCommand.createCommand();
        }

        public static void addCommandToConsole(string name, AbstractCommand abstractCommand)
        {
            if (!commands.ContainsKey(name))
            {
                commands.Add(name,abstractCommand);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                canvas.gameObject.SetActive(!canvas.gameObject.activeInHierarchy);
                if (Cursor.lockState == CursorLockMode.Locked)
                {
                    Cursor.lockState = CursorLockMode.None;
                }
                else
                {
                    Cursor.lockState = CursorLockMode.Locked;
                }
            }

            if (canvas.gameObject.activeInHierarchy)
            {
                if (Input.GetKeyDown(KeyCode.Return)){
                    if(inputText.text != "")
                    {
                        addMessage(inputText.text);
                        parseInput(inputText.text);
                    }
                }
                inputField.Select();
               
            }
        }

        public void addMessage(string message)
        {
            consoleText.text += message + "\n";
        }

       

        private void parseInput(string input)
        {
            string[] validInput = input.Split(null);
            if(validInput.Length == 0 || validInput == null)
            {
                Debug.LogWarning("Command not found!");
                return;
            }

            if (!commands.ContainsKey(validInput[0]))
            {
                Debug.LogWarning("Command: "+validInput[0]+" not found!");
            }
            else
            {
                commands[validInput[0]].runCommand();
            }
        }
    }
}
