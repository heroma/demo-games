﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PictureMaker : MonoBehaviour{
	
	/*-------------------------------------------
	Takes a photo of the transferring gameobject
	---------------------------------------------*/
	
	private static PictureMaker _instance;
	public static PictureMaker Instance { get { return _instance; } }

	[SerializeField]
	private Camera pictureCamera;
	[SerializeField]
	private Transform puk;
	[SerializeField]
	private Transform showPoint;
	[SerializeField]
	private Vector3 pukPosition;
	[SerializeField]
	private Vector3 pukRotation;
	[SerializeField]
	private bool background = false;
	[SerializeField]
	private Color backgroungColor = Color.black;
	private bool save = false;
	
	private int pic_width=256;
	private int pic_height = 256;
	private int pic_depth = 24;
	
	private bool makePic = false;
	private Transform oldTransform;
	private Vector3 oldPositon;
	private Quaternion oldRotation;
	private GameObject pictureObject;
	
	private void Awake(){
		if (_instance != null && _instance != this){
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
		GetComponent<Camera>().gameObject.SetActive(false);
	}

	public Sprite getSpriteFromObject(GameObject gameObject,bool save){
		
		if(background){
			GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
		}else{
			GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
			GetComponent<Camera>().backgroundColor = backgroungColor;
		}
		this.save = save; 
		pictureObject = gameObject;
		makePic = true;
		GetComponent<Camera>().gameObject.SetActive(true);
		puk.transform.localPosition = pukPosition;
		puk.transform.rotation = Quaternion.identity;
		return renderTexture();
		
	}
	
	public void takePictureFromObject(GameObject gameObject){
		pictureObject = gameObject;
		makePic = true;
		GetComponent<Camera>().gameObject.SetActive(true);
		renderTexture();
	}
	
	private void placeGameObject(GameObject gameObject){
		oldTransform = gameObject.transform.parent;
		oldPositon = gameObject.transform.position;
		oldRotation = gameObject.transform.rotation;
		gameObject.transform.parent = puk;
		gameObject.transform.localRotation = Quaternion.identity;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.LookAt(showPoint);
		puk.transform.localRotation = new Quaternion(pukRotation.x,pukRotation.y,pukRotation.z,0);
	}
	
	private void replaceObject(GameObject gameObject){
		gameObject.transform.parent =  oldTransform;
		gameObject.transform.position = oldPositon;
		gameObject.transform.rotation = oldRotation;
	}
	
	private Sprite renderTexture(){
		Sprite sprite = null;
		if(makePic && pictureObject != null){
			placeGameObject(pictureObject);
			if(puk.childCount > 0 && puk.GetChild(0)){
				sprite	= createPicture(puk.GetChild(0).gameObject);
			}
			replaceObject(pictureObject);
		}
		return sprite;
	}
	
	private Sprite createPicture(GameObject gameObject){
		if(makePic && GetComponent<Camera>().gameObject.activeInHierarchy){
			GetComponent<Camera>().targetTexture = new RenderTexture(pic_width,pic_height,pic_depth);
			Texture2D pic = new Texture2D(pic_width,pic_height,TextureFormat.RGB24,false);
			GetComponent<Camera>().Render();
			RenderTexture.active = GetComponent<Camera>().targetTexture;
			pic.ReadPixels(new Rect(0,0,pic_width,pic_height),0,0);
			byte[] byts =	pic.EncodeToPNG();
			if(save){
				System.IO.File.WriteAllBytes(createPicName(gameObject),byts);
			}
			makePic = false;
			GetComponent<Camera>().gameObject.SetActive(false);
			return pictureToSprite(byts);
			
		}
		return null;
	}
	
	private Sprite pictureToSprite(byte[] fileData){
		Texture2D texture = new Texture2D(2,2);
		if(texture.LoadImage(fileData)){
			return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0), 100, 0, SpriteMeshType.FullRect);
		}
		throw new System.Exception("Fehler beim erstellen des Sprites");
	}
	
	
	private string createPicName(GameObject gameObject){
		return string.Format("{0}/PictureMaker/Pictures/snap_id[{1}].png",Application.dataPath,gameObject.name); 
	}
}
