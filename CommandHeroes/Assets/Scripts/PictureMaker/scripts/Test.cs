﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Test : MonoBehaviour
{
	[SerializeField]
	private GameObject go1;
	[SerializeField]
	private GameObject go2;
	[SerializeField]
	Button button;

    // Update is called once per frame
    void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space)){
			Sprite sprite = PictureMaker.Instance.getSpriteFromObject(go2,false);
			button.GetComponent<Image>().sprite = sprite;
			button.image.sprite = sprite;
	    }
    }
}
