﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTools{
	
    public class CameraUtils{
    
	    public static Vector3 getMouseWorldPosition(){
            Vector3 vector3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            vector3.z = 0f;
            return vector3;
        }
    }
}
