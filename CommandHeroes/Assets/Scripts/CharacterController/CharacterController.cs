﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
	private const string mageId = "micro_wizard";
	private const string elfId = "micro_druid";
	private const string knightId = "micro_knight";
	
	[SerializeField]
	private float speed;
	[SerializeField]
	private float jumpForce;
	[SerializeField]
	private int extraJump=1;
	
	private bool isFacingRight;
	
	private float moveInput;
	private int def_extraJump=1;
	private bool isGrounded;
	[SerializeField]
	private Transform groundCheck;
	[SerializeField]
	private float checkRadius;
	[SerializeField]
	private LayerMask whatIsGround;
	
	
	private Rigidbody rig;

	// Awake is called when the script instance is being loaded.
	protected void Awake(){
		rig = GetComponent<Rigidbody>();
		def_extraJump = extraJump;
		
	}

   
	public void jump(float jumpHeight){
		if(def_extraJump > 0){
			rig.velocity = Vector3.up * (jumpForce+jumpHeight);
			def_extraJump--;
		}
	}
   
	public void walkJump(){
		move_right();
		jump(1);
	}
   
	private void grounded(){
		if(isGrounded){
			def_extraJump = extraJump;
		}
	}
	private void flip(){
		isFacingRight = !isFacingRight;
		Vector3 scaler = transform.localScale;
		scaler.x *=-1;
	}
   
	public void runSpecial(){
		if(transform.name == mageId ){
			Debug.Log("Special <Mage>");
			doSpecial();
		}
		if(transform.name == elfId ){
			Debug.Log("Special <Elf>");
			doSpecial();
		}
		if(transform.name == knightId ){
			Debug.Log("Special <Knight>");
			doSpecial();
		}
		
	
	}
   
	public virtual void doSpecial(){
		// not implemented
	}
	
	protected void FixedUpdate(){
		isGrounded = Physics.OverlapSphere(groundCheck.position,checkRadius,whatIsGround).Length > 0;
		
		rig.velocity = new Vector3(moveInput * (speed),rig.velocity.y,rig.velocity.z);
		
		if(isFacingRight == false && moveInput > 0){
			flip();
		}else if(isFacingRight == true && moveInput < 0){
			flip();
		}
		
		grounded();
	}
	
	public void move_right(){
		moveInput = 1;
	}
	
	public void move_left(){
		moveInput = -1;
	}
	
	protected void Update(){
		grounded();
	}
	
	public void stopWalking(){
		moveInput = 0;
		rig.velocity = Vector3.zero;
	}
	
	public void stopJumping(){
		moveInput = 0;
	}
}
