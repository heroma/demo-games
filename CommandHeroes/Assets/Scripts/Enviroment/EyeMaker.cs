﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTools;
public class EyeMaker : MonoBehaviour
{
	[SerializeField]
	private int eyeCount;
	
	[SerializeField]
	private float timerAktivate = 1f;
	
	private List<GameObject> eyePrefaps;
	
	
	private float timer=0;
	private GameObject oldSel;
	private GameObject newSel;
  
    void Start() {
	    eyePrefaps = new List<GameObject>();
	    GameObject go = null;
	    for (int i = 0; i < eyeCount; i++) {
			
		    go = AssetFactory.loadAssetFromDatabase("eyes");
		    float circleposition = (float)i / (float)eyeCount;
		    float x = Mathf.Sin( circleposition * Mathf.PI * 2.0f ) * 1;
		    float y = Mathf.Cos( circleposition * Mathf.PI * 2.0f ) * 1;
		    
		    go.transform.position = new Vector3(x,y,-8.9f);
		    go.transform.parent = transform;
		    eyePrefaps.Add(go);
		}
    }

    
    void Update(){
	    timer += Time.deltaTime;
	    if((timer%60) >= timerAktivate){
	    	if(oldSel != null){
		    	oldSel.SetActive(true);
	    	}
	    	oldSel = newSel;
	    	newSel = eyePrefaps[Random.Range(0,eyeCount)];
		    newSel.SetActive(false);	
	    	timer=0;
	    }
    }
}
