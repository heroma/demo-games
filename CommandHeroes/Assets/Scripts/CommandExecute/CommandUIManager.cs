﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq; 
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameTools;
namespace CommandSystem {
    public class CommandUIManager : MonoBehaviour {

	    /*---------------------------------------
	    	Command display and control 
	    -----------------------------------------*/

        [SerializeField]
	    private int selectableComSize = 4;

        [SerializeField]
        private GameObject buttonPrefap;

	    [SerializeField]
	    private GameObject buttonPrefap_info;

        [SerializeField]
	    private Transform buttonPanel;
       
	    private Dictionary<String,Transform> selectedActions = new Dictionary<string, Transform>();

        [SerializeField]
        private Transform runButton;

        private GameObject[] selectableButtons;
	    private Dictionary<String,Dictionary<GameObject,CommandDatabase.Command>> selectedCommandos;

        private CommandManager commandManager;
	    [SerializeField]
	    private Transform trash;

	    private List<GameObject> markerList;
	    private Dictionary<String,List<GameObject>> markerDic;
	    
	    private const float MARKER_DISTANCE = 0.4f; 
	    
	    public void addUI(string id, Transform ui){
	    	selectedActions.Add(id,ui);
	    	selectedCommandos[id] = new Dictionary<GameObject, CommandDatabase.Command>();
	    }

        public void setCommandManger(CommandManager commandManager){
	        this.commandManager = commandManager;
	    }
	   
	    protected void Awake(){
		    selectedCommandos = new Dictionary<String,Dictionary<GameObject,CommandDatabase.Command>>();
		    markerList = new List<GameObject>();
		    markerDic = new Dictionary<string,List<GameObject>>();
		    selectableButtons = new GameObject[selectableComSize];

            runButton.GetComponent<Button>().onClick.AddListener(() => processSelectedComandos());
		    buttonPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(100 * selectableComSize,100);
           
            for (int i = 0; i < selectableComSize; i++){
	            selectableButtons[i] = Instantiate(buttonPrefap, buttonPanel);
	        }
	    }
	    
	    protected void Update(){
		    if(GameManager.isActionRunning()){
		    	Cursor.visible = false;
		    	runButton.gameObject.SetActive(false);
		    }else{
		    	Cursor.visible = true;
		    	runButton.gameObject.SetActive(true);
		    }   	
	    }

        public void setIconsForCommand(List<CommandDatabase.Command> commands){
		    if(selectableButtons != null){
		
			    for (int i = 0; i < selectableComSize; i++){
                	selectableButtons[i].GetComponent<Button>().image.sprite = commands[i].action_icon;
	            	selectableButtons[i].GetComponent<ButtonAction>().commandUIManager = this;
            	    selectableButtons[i].GetComponent<ButtonAction>().command = commands[i];
            	}
        	}
	    }
	    
	    public void addToSelectedCommandos(String id,CommandDatabase.Command command){
	    	if(id == null || command == null)
	    		return;
	    		
			if(selectedActions != null){
		    	GameObject sel = Instantiate(buttonPrefap_info, selectedActions[id]);
	            sel.GetComponent<Button>().image.sprite = command.action_icon;
		    	sel.GetComponent<Button>().name = IdGenerator.createId(12);
		    	sel.GetComponent<Button>().onClick.AddListener(delegate {clearCommando(id,sel.GetComponent<Button>().name);});
				sel.GetComponent<Button>().interactable = false;
		    	selectedCommandos[id].Add(sel, command);
		        selectedActions[id].parent.GetComponent<RectTransform>().sizeDelta = new Vector2(50 * selectedCommandos.Keys.Count,50);
				createMarker(id,selectedCommandos[id].Count -1,sel.GetComponent<Button>().name,command.playerActionName);
			}else{
	    		Debug.LogError("Die ID["+id+"] ist nicht enthalten!");
	    	}
        }

	    
	    private void createMarker(string id,int index,String buttonId,String actionId){
	    	Vector3 playerPos = CommandManager.getPlayer().position;
	    	GameObject marker = AssetFactory.loadAssetFromDatabase("marker");
		    setMarkerColorByName(marker);
		    marker.transform.name = buttonId;
		    marker.GetComponent<Marker>().setActionId("");
		    marker.transform.SetParent(GameManager.getTrash());
		    if(selectedCommandos[id] != null && selectedCommandos[id].Values != null && selectedCommandos[id].Values.ToArray()[index] != null){
		    	switch (selectedCommandos[id].Values.ToArray()[index].playerActionName){
		    		case "WalkR":{
			    		float pos = (MarkerManager.getCountRight(id) * MARKER_DISTANCE) + playerPos.x;
			    		marker.transform.position = new Vector3(marker.transform.position.x+pos,playerPos.y+1,marker.transform.position.z);  		
				   		MarkerManager.increaseCountRight(id);
				   		break;
			    	}
		    		case "WalkL":{
		    			float pos = (MarkerManager.getCountLeft(id) * MARKER_DISTANCE) - playerPos.x;
			    		marker.transform.position = new Vector3(marker.transform.position.x-pos,playerPos.y+1,marker.transform.position.z);
			    		MarkerManager.increaseCountLeft(id);
			    		break;	
		    		}
		    		case "Jump":{
		        		marker.transform.position = new Vector3(marker.transform.position.x,playerPos.y+2,marker.transform.position.z);
			    		break;	
		    		}
		    		case "Walk_Jump":{
		    			float pos = (selectedCommandos[id].Count * MARKER_DISTANCE) + playerPos.x;
			    		marker.transform.position = new Vector3(marker.transform.position.x+pos,playerPos.y+1,marker.transform.position.z);  	
			    		MarkerManager.increaseCountRight(id);
			    		break;
		    		}
		    	}
		    
		  
		    	RaycastHit hit;
				if(Physics.Raycast(marker.transform.position,Vector3.down,out hit,3f )){
		    		marker.transform.position = new Vector3(marker.transform.position.x,hit.point.y,marker.transform.position.z);
				}
			    if(markerDic.ContainsKey(id)){
				    ((List<GameObject>)markerDic[id]).Add(marker);
			    }else{
			    	List<GameObject> goList = new List<GameObject>();
			    	goList.Add(marker);
			    	markerDic.Add(id,goList);
			    }
	    	}
	    }

	    private void setMarkerColorByName(GameObject marker){
		    Renderer renderer = marker.gameObject.GetComponent<Renderer>();

		    if(CommandManager.getPlayer().name.Contains("micro_druid")){
			    renderer.material.SetColor("_Color",Color.green);
			    renderer.material.SetColor("_Color2",Color.green);
		    }else if(CommandManager.getPlayer().name.Contains("micro_knight")){
		    	renderer.material.SetColor("_Color",Color.yellow);
			    renderer.material.SetColor("_Color2",Color.yellow);
		    }else if(CommandManager.getPlayer().name.Contains("micro_wizard")){
		    	renderer.material.SetColor("_Color",Color.red);
			    renderer.material.SetColor("_Color2",Color.red);
		    }
	    }

	    //--------------------------------------------------
	    // Play the Action 
	    //--------------------------------------------------
	    public void processSelectedComandos(){
		    if(!GameManager.isActionRunning()){
	        	foreach (KeyValuePair<String,Dictionary<GameObject,CommandDatabase.Command>> item in selectedCommandos){
			    	if(item.Value.Values != null && item.Value.Values.Count > 0){
		        		commandManager.runPlayerAction(item.Key,item.Value.Values);
		    		}	
	        	}
	       }
	    }

	    //--------------------------------------------------
	    // remove selected commandos
	    //--------------------------------------------------
	    public void removeCommandSelectedCommandos(String id,CommandDatabase.Command command){
		    foreach (KeyValuePair<GameObject,CommandDatabase.Command> com in selectedCommandos[id]) {
			    if (com.Value.id.Equals(command.id) && selectedActions.ContainsKey(id)){
	                Transform transform = selectedActions[id].Find(com.Key.name);
	                if(transform != null){
		                transform.gameObject.SetActive(false);
	                }else{
	                	Debug.LogError("Transform is Null!");
	                }
                }
		    }
	    }
	    
	    //--------------------------------------------------
	    // clear selected commandos
	    //--------------------------------------------------
	    public void clearList(string id){
	    	Dictionary<String,Dictionary<GameObject, CommandDatabase.Command>> newSelectedCommandos = new Dictionary<string, Dictionary<GameObject, CommandDatabase.Command>>(selectedCommandos);
	    	Dictionary<GameObject, CommandDatabase.Command> selectedCommndosById = new Dictionary<GameObject, CommandDatabase.Command>(selectedCommandos[id]);
	    	
	    	foreach (KeyValuePair<GameObject, CommandDatabase.Command> com in selectedCommndosById){
		    	com.Key.GetComponent<Image>().sprite = null;
		    	com.Key.transform.SetParent(GameManager.getTrash());
            }
		    selectedCommndosById.Clear();
			
		    newSelectedCommandos[id] = selectedCommndosById;	
		    selectedCommandos = newSelectedCommandos;
		    
		    markerDic[id].Clear();
        }

	    //--------------------------------------------------
	    // delete Command from Char by comandId
	    //--------------------------------------------------
	    public void clearCommando(string charId,String commandId){
		    Dictionary<String,Dictionary<GameObject, CommandDatabase.Command>> newSelectedCommandos = new Dictionary<string, Dictionary<GameObject, CommandDatabase.Command>>(selectedCommandos);
		    
	    	foreach (KeyValuePair<GameObject, CommandDatabase.Command> com in newSelectedCommandos[charId]){
	    		if(com.Key.name.Equals(commandId)){
		    		Debug.Log("Clear commandId: "+commandId + " com.Key.name: "+com.Key.name);
		    		Transform transform = selectedActions[charId].Find(com.Key.name);
		    		if(transform != null){
			    		transform.gameObject.SetActive(false);
			    		transform.SetParent(GameManager.getTrash());
			    	}
	    		}
	    	}
	    	
			foreach(GameObject marker in markerDic[charId]){
		    	if(marker.name.Equals(commandId)){
			    	if(marker.GetComponent<Marker>().getActionId().Equals("Walk Right") || marker.GetComponent<Marker>().getActionId().Equals("Walk_Jump")){
			    		MarkerManager.decreaseCountRight(charId);
			    	}
			    	if(marker.GetComponent<Marker>().getActionId().Equals("Walk Left")){
			    		MarkerManager.decreaseCountLeft(charId);
			    	}
			    	
			    	marker.transform.gameObject.SetActive(false);
			    	marker.transform.SetParent(GameManager.getTrash());
			 	}
	    	}
	    	
		    selectedCommandos = newSelectedCommandos;
	    }
    }
}
