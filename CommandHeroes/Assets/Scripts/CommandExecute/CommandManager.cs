﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CommandSystem
{
	public class CommandManager : MonoBehaviour {
    	
		/*--------------------------------------------------------
			Main class that takes care of the command processing
		----------------------------------------------------------*/
    	
    	
        private List<CommandDatabase.Command> comandList;
	    private static CommandUIManager commandUI;
	    
	    [SerializeField]
	    private static Transform player;

        [SerializeField]
        private string[] initalCommand;
	  
	    protected void Awake(){
		    comandList = new List<CommandDatabase.Command>();
		    commandUI = GetComponent<CommandUIManager>();
		    commandUI.setCommandManger(this);
	  }
	    
	    protected void Start(){
		    loadCommands();
            commandUI.setIconsForCommand(comandList);
        }

        private void loadCommands(){
            foreach (string commandID in initalCommand){
                comandList.Add(CommandDatabase.instance.getCommand(commandID));
            }
        }

	    private IEnumerator fireCommands(string id,Dictionary<GameObject, CommandDatabase.Command>.ValueCollection values){
		    Transform player = GameManager.getFigure(id);
		    GameManager.setActionIsRunning(id);
		    foreach (CommandDatabase.Command command in values){
		  
		        switch (command.playerActionName){
                	case "WalkR":{
	                	player.GetComponent<CharacterController>().move_right();
	                	player.GetComponent<Animator>().SetBool("walk",true);
		                yield return new WaitForSeconds(command.playTime);
		                player.GetComponent<CharacterController>().stopWalking();
		                commandUI.removeCommandSelectedCommandos(GameManager.getSelectedId() ,command);
		                GameManager.increasCounter();
		                break;
	                }
            		case "WalkL":{
	                	player.GetComponent<CharacterController>().move_left();
	                	player.GetComponent<Animator>().SetBool("walk",true);
		                yield return new WaitForSeconds(command.playTime);
		                player.GetComponent<CharacterController>().stopWalking();
	            		commandUI.removeCommandSelectedCommandos(GameManager.getSelectedId(),command);
	            		GameManager.increasCounter();
		                break;
	                }
            		case "Jump":{
	            		player.GetComponent<CharacterController>().jump(1f);
	                    yield return new WaitForSeconds(command.playTime);
		            	commandUI.removeCommandSelectedCommandos(GameManager.getSelectedId(),command);
		            	GameManager.increasCounter();
                	    break;
                    }
            		case "Walk_Jump":{
		            	player.GetComponent<CharacterController>().walkJump();
		            	player.GetComponent<Animator>().SetBool("walk",true);
		            	yield return new WaitForSeconds(command.playTime);
	        		   	commandUI.removeCommandSelectedCommandos(GameManager.getSelectedId(),command);
	            		player.GetComponent<CharacterController>().stopJumping();
	            		GameManager.increasCounter();
		        	    break;
            		}
                	case "Special":{
	                	player.GetComponent<CharacterController>().runSpecial();
	                	yield return new WaitForSeconds(command.playTime);
	                	commandUI.removeCommandSelectedCommandos(GameManager.getSelectedId(),command);
	                	player.GetComponent<CharacterController>().stopWalking();
	                	GameManager.increasCounter();
	                	break;
                	}
                }
		    }
	        Debug.Log("Finish: <"+player.name+">");
		    commandUI.clearList(id);
		    GameManager.setActionIsStop(id);
	    }

        public List<CommandDatabase.Command> getCommands(){
            return comandList;
        }

	    internal void runPlayerAction(string id,Dictionary<GameObject, CommandDatabase.Command>.ValueCollection values){
	        StartCoroutine(fireCommands(id,values));
        }
        
	    public static void setPlayer(string id){
	    	player = GameManager.getFigure(id);
	    }
	    
	    public static Transform getPlayer(){
	    	return player;
	    }
	    
	    public static CommandUIManager getUiManager(){
	    	return commandUI;
	    }
    }
}
