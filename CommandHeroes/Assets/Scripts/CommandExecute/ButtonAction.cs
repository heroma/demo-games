﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GameTools;
namespace CommandSystem {
    public class ButtonAction : MonoBehaviour{
        
	    public CommandUIManager commandUIManager;
        public CommandDatabase.Command command;

        private void Awake(){
	        GetComponent<Button>().onClick.AddListener(() => fireEvent());
	        commandUIManager = CommandManager.getUiManager();
	    }

        public void fireEvent(){
	    	if(commandUIManager != null){
			    Debug.Log("CommandID[" + command.id + "] ActionId[" + this.command.playerActionName + "]");
		        Debug.Log("GameManager.getSelectedId(): "+GameManager.getSelectedId());
			    commandUIManager.addToSelectedCommandos(GameManager.getSelectedId(),this.command);
	        }
        }
    }
}