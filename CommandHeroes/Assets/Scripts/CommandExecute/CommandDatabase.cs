﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommandSystem {
	
	//-----------------------------------------------
	// manage the commands 
	//-----------------------------------------------
	
    public class CommandDatabase : MonoBehaviour{

        [Serializable]
        public class Command{
            public string name;
            public string id;
            public Sprite action_icon;
            public string playerActionName;
            public Animation animation;
            public float playTime;
            public string messageId;
	    }

        [SerializeField]
        private Command[] commands;

        private static CommandDatabase _instance;
        public static CommandDatabase instance {
            get {
                if (_instance == null){
                    _instance = Instantiate(Resources.Load<CommandDatabase>("CommandDatabase"));
                }
                return _instance;
            }
        }

        public Command getCommand(string id){
            foreach(Command command in commands){
                if(command.id.Equals(id)){
                    Debug.Log("Found in CommandDB["+id+"]");
                    return command;
                }
            }
            Debug.LogError("ID [" + id + "] in CommandDatabase not found!");
            return null;
        }
    }
}