﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class PartikelSystem : MonoBehaviour
{

    public static PartikelSystem instance;

    private Mesh mesh;
    private Vector3[] vertices;
    private Vector2[] uvs;
    private int[] triangles;
    private int quad_index;
    private const int MAX_VERTICE = 1500;

    private void Awake()
    {
        instance = this;
        mesh = new Mesh();
        vertices = new Vector3[4 * MAX_VERTICE];
        uvs = new Vector2[4 * MAX_VERTICE];
        triangles = new int[6 * MAX_VERTICE];
       
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        GetComponent<MeshFilter>().mesh = mesh;
    }

    public void spawnPartikel(PartikelSystemArgs args)
    {
        int spawnQuadIndex = createQuad(args.position, args.rotation,args.size,args.material);
    }

    private int createQuad(Vector3 position,float rotation, Vector3 quadSize,Material material)
    {
        if (quad_index > MAX_VERTICE)
            return 0;

        QuadUpdate(quad_index, position, rotation, quadSize, material);
        int spawnIndex = quad_index; 
        quad_index++;

        return spawnIndex;
    }

    public void QuadUpdate(int quadInex,Vector3 quadPosition, float rotation, Vector3 quadSize, Material material)
    {
        int vIndex = quad_index * 4;
        int vIndex0 = vIndex;
        int vIndex1 = vIndex + 1;
        int vIndex2 = vIndex + 2;
        int vIndex3 = vIndex + 3;

        vertices[vIndex0] = quadPosition + Quaternion.Euler(0, 0, rotation - 180) * quadSize;
        vertices[vIndex1] = quadPosition + Quaternion.Euler(0, 0, rotation - 270) * quadSize;
        vertices[vIndex2] = quadPosition + Quaternion.Euler(0, 0, rotation - 0) * quadSize;
        vertices[vIndex3] = quadPosition + Quaternion.Euler(0, 0, rotation - 90) * quadSize;

        uvs[vIndex0] = new Vector2(0, 0);
        uvs[vIndex1] = new Vector2(0, 1);
        uvs[vIndex2] = new Vector2(1, 1);
        uvs[vIndex3] = new Vector2(1, 0);

        int tIndex = quad_index * 6;
        triangles[tIndex + 0] = vIndex0;
        triangles[tIndex + 1] = vIndex1;
        triangles[tIndex + 2] = vIndex2;
        triangles[tIndex + 3] = vIndex0;
        triangles[tIndex + 4] = vIndex2;
        triangles[tIndex + 5] = vIndex3;

        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        GetComponent<MeshRenderer>().material = material;

    }

    public class PartikelSystemArgs : EventArgs { 
        public Material material;
        public Vector3 position;
        public float rotation;
        public Vector3 size;

        public PartikelSystemArgs(Vector3 position,float rotation,Vector3 size,Material material)
        {
            this.material = material;
            this.position = position;
            this.rotation = rotation;
            this.size = size;
        }    
    }
}

 